package com.test.feedswithtests.router

import android.content.Context
import android.content.Intent
import com.test.feedswithtests.controllers.DetailsActivity
import com.test.feedswithtests.models.Feed
import dagger.hilt.android.qualifiers.ActivityContext
import dagger.hilt.android.scopes.ActivityScoped
import javax.inject.Inject

interface EARouter{
    fun details(feed: Feed)
}

@ActivityScoped
class EARouterImpl @Inject constructor(@ActivityContext private val activity: Context) : EARouter {

    override fun details(feed: Feed){
        val intent = Intent(activity, DetailsActivity::class.java)
        intent.putExtra("feed", feed)
        activity.startActivity(intent)
    }

}