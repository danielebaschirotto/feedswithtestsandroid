
package com.test.feedswithtests.router

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@InstallIn(ActivityComponent::class)
@Module
abstract class RouterModule {

    @Binds
    abstract fun providesRouter(impl: EARouterImpl): EARouter
}