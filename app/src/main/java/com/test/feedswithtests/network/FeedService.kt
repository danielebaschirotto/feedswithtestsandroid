package com.test.feedswithtests.network

import com.test.feedswithtests.models.Feed
import retrofit2.Call
import retrofit2.http.GET

interface FeedService {
    @GET("feeds.json")
    fun getFeeds() : Call<List<Feed>>
}