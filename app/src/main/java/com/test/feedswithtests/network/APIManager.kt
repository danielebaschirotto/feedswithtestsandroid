package com.test.feedswithtests.network

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.SocketTimeoutException
import javax.inject.Inject

interface APIManager {

    fun getFeeds(responseCallback: (Int, Any?, String?, String) -> Unit)

}

class APIManagerImpl @Inject constructor() : APIManager {
    private var feedService : FeedService? = null
    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://danielebaschirotto.altervista.org/intesys/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
         feedService = retrofit.create(FeedService::class.java)
    }

    private fun <T> execute(call: Call<T>?, responseCallback: (Int, Any?, String?, String) -> Unit) {
        call?.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                var errorObject: String? = null
                if (response.errorBody() != null) {
                    try {
                        errorObject = response.errorBody()?.string()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                responseCallback(response.code(), response.body(), errorObject, response.message())
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                //Log.d("error", t.getMessage());
                if (t is SocketTimeoutException) {
                    responseCallback(-10, null, null, "")
                } else responseCallback(-1, null, null, "")
            }
        })
    }

    override fun getFeeds(responseCallback: (Int, Any?, String?, String) -> Unit){
        val call = feedService?.getFeeds()
        execute(call, responseCallback)
    }
}