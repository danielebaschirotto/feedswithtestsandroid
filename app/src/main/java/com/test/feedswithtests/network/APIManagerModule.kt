
package com.test.feedswithtests.network

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@InstallIn(ViewModelComponent::class)
@Module
abstract class APIManagerModule {

    @Binds
    abstract fun providesAPIManager(impl: APIManagerImpl): APIManager
}