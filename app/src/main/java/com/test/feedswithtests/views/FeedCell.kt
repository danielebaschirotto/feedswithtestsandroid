package com.test.feedswithtests.views

import androidx.recyclerview.widget.RecyclerView
import com.test.feedswithtests.imageLoader.ImageLoader
import com.test.feedswithtests.models.Feed
import com.test.feedswithtests.databinding.ItemFeedBinding

class FeedCell(val binding: ItemFeedBinding, val imageLoader: ImageLoader, val clickAction: (Feed)->Unit) : RecyclerView.ViewHolder(binding.root){

    fun bind(item: Feed) {
        with(binding) {
            location.text = item.location
            var desc = item.description
            if(desc.length > 143) desc = desc.substring(0,140)
            description.text = desc +"..."
            card.setOnClickListener { clickAction(item) }
            imageLoader.loadInto(item.image, itemImage)
        }
    }
}