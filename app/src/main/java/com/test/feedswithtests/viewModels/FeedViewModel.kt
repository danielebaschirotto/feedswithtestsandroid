package com.test.feedswithtests.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.test.feedswithtests.models.Feed
import com.test.feedswithtests.models.Resource
import com.test.feedswithtests.network.APIManager
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class FeedViewModel @Inject constructor(
) : ViewModel() {
    @Inject lateinit var apiManager: APIManager

    fun getFeeds(): LiveData<Resource<List<Feed>>> {
        val result = MutableLiveData<Resource<List<Feed>>>()
        apiManager.getFeeds { code, body, errorBody, message ->
            val resource = Resource(code, body as List<Feed>?, errorBody, message)
            result.setValue(resource)
        }
        return result
    }

}