package com.test.feedswithtests.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.test.feedswithtests.imageLoader.ImageLoader
import com.test.feedswithtests.models.Feed
import com.test.feedswithtests.views.FeedCell
import com.test.feedswithtests.databinding.ItemFeedBinding

class FeedsAdapter(
    private val activity: Activity,
    private val imageLoader: ImageLoader,
    private val items: List<Feed>,
    private val clickAction: (Feed) -> Unit = {}
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeedCell {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemFeedBinding.inflate(inflater)
        return FeedCell(binding, imageLoader, clickAction)
    }

    fun withRecyclerView(rview: RecyclerView) {
        rview.isFocusable = false
        rview.adapter = this
        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        rview.layoutManager = layoutManager
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = (holder as FeedCell).bind(items[position])

}