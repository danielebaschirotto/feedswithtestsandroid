package com.test.feedswithtests.models

class Resource<T>(statusCode:Int, data:T?, errorData:Any?, message:String) {

    @kotlin.jvm.JvmField
    var statusCode: Int = 0
    @kotlin.jvm.JvmField
    var data: T? = null
    @kotlin.jvm.JvmField
    var errorData:Any? = null
    @kotlin.jvm.JvmField
    var message:String = ""

    init{
        this.statusCode = statusCode
        this.data = data
        this.errorData = errorData
        this.message = message
    }

    constructor(statusCode:Int, data:T?, message:String) : this(statusCode, data, null, message)

}
