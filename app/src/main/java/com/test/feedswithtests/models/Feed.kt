package com.test.feedswithtests.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Feed(
    var location : String,
    var description : String,
    var image : String): Parcelable