package com.test.feedswithtests.imageLoader

import android.widget.ImageView
import com.squareup.picasso.Picasso
import javax.inject.Inject

interface ImageLoader{
    fun loadInto(url : String, view : ImageView)
}

class ImageLoaderImpl @Inject constructor() : ImageLoader {

    override fun loadInto(url: String, view: ImageView) {
        Picasso.get().load(url).into(view)
    }

}