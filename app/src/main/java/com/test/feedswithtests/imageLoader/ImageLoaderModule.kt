
package com.test.feedswithtests.imageLoader

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent

@InstallIn(ActivityComponent::class)
@Module
abstract class ImageLoaderModule {

    @Binds
    abstract fun providesLoader(impl: ImageLoaderImpl): ImageLoader
}