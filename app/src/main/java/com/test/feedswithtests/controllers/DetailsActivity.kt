package com.test.feedswithtests.controllers

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.test.feedswithtests.imageLoader.ImageLoader
import com.test.feedswithtests.models.Feed
import com.test.feedswithtests.router.EARouter
import com.test.feedswithtests.databinding.ActivityDetailsBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DetailsActivity : AppCompatActivity() {

    lateinit var binding: ActivityDetailsBinding

    @Inject lateinit var router: EARouter
    @Inject lateinit var imageLoader: ImageLoader

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.toolbar.setNavigationOnClickListener {
            finish()
        }

        val feed = intent.getParcelableExtra<Feed>("feed")

        setViewWithFeed(feed)
    }

    private fun setViewWithFeed(feed : Feed?){
        feed?.let {
            binding.description.text = it.description
            binding.location.text = it.location
            imageLoader.loadInto(it.image, binding.image)
        }
    }
}