package com.test.feedswithtests.controllers

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.test.feedswithtests.adapters.FeedsAdapter
import com.test.feedswithtests.imageLoader.ImageLoader
import com.test.feedswithtests.models.Feed
import com.test.feedswithtests.router.EARouter
import com.test.feedswithtests.viewModels.FeedViewModel
import com.test.feedswithtests.databinding.ActivityFeedsBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FeedsActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener {
    @Inject lateinit var router: EARouter
    @Inject lateinit var imageLoader: ImageLoader

    val feedViewModel: FeedViewModel by viewModels()

    lateinit var binding: ActivityFeedsBinding
    private lateinit var feedsAdapter: FeedsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFeedsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        updateFeeds()

    }

    private fun updateFeeds(){
        binding.swipe.isRefreshing = true
        binding.swipe.setOnRefreshListener(this)
        feedsAdapter = FeedsAdapter(this, imageLoader, listOf())
        feedsAdapter.withRecyclerView((binding.feedsList))
        feedViewModel.getFeeds().observe(this,  { result ->
            result.data?.let { items ->
                feedsAdapter = FeedsAdapter(this, imageLoader, items)
                { item: Feed ->
                    router.details(item)
                }
                feedsAdapter.withRecyclerView((binding.feedsList))
            }
            binding.swipe.isRefreshing = false
        })
    }

    override fun onRefresh() {
        updateFeeds()
    }
}