
package com.test.feedswithtests.imageLoader

import dagger.Binds
import dagger.Module
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.testing.TestInstallIn

@Module
@TestInstallIn(
    components = [ActivityComponent::class],
    replaces = [ImageLoaderModule::class]
)
abstract class TestImageLoaderModule {

    @Binds
    abstract fun providesImageLoader(impl: ImageLoaderTest): ImageLoader
}