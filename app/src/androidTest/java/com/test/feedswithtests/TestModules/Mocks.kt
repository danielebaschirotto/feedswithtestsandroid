package com.test.feedswithtests.TestModules

import com.test.feedswithtests.models.Feed

fun anyString() : String { return "Any String" }

fun anyOtherString() : String { return "Any OTHER String" }

fun anyURL() : String { return "https://a.com" }
fun anyOtherURL() : String { return "https://b.com" }

fun anyFeed(): Feed {
    return Feed(anyString(), anyOtherString(), anyURL())
}

fun anyOtherFeed(): Feed {
    return Feed(anyOtherString(),anyString(),anyOtherURL())
}

fun anyFeedList(): List<Feed> {
    return listOf<Feed>(anyFeed(), anyOtherFeed())
}