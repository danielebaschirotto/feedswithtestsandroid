
package com.test.feedswithtests.TestModules

import com.test.feedswithtests.network.APIManager
import com.test.feedswithtests.network.APIManagerModule
import dagger.Binds
import dagger.Module
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.testing.TestInstallIn

@Module
@TestInstallIn(
    components = [ViewModelComponent::class],
    replaces = [APIManagerModule::class]
)
abstract class TestAPIManagerModule {

    @Binds
    abstract fun providesAPIManager(impl: APIManagerTest): APIManager
}