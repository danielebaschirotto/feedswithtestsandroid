package com.test.feedswithtests.TestModules

import com.test.feedswithtests.models.Feed
import com.test.feedswithtests.router.EARouter
import javax.inject.Inject


class EARouterSpy @Inject constructor() : EARouter {

    companion object {
        var messages: MutableList<String> = mutableListOf()
    }

    /*override fun navigate(activity:Activity, to: String){
        Log.d("Tests",to)
        messages.add(to)
    }*/

    override fun details(feed: Feed) {
        messages.add("details")
    }
}