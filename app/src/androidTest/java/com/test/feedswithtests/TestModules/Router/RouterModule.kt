
package com.test.feedswithtests.TestModules

import com.test.feedswithtests.router.EARouter
import com.test.feedswithtests.router.RouterModule
import dagger.Binds
import dagger.Module
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.testing.TestInstallIn

@Module
@TestInstallIn(
    components = [ActivityComponent::class],
    replaces = [RouterModule::class]
)

abstract class TestRouterModule {

    @Binds
    abstract fun providesRouter(impl: EARouterSpy): EARouter
}