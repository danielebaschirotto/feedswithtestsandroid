package com.test.feedswithtests.imageLoader

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.widget.ImageView
import androidx.core.view.drawToBitmap
import com.test.feedswithtests.TestModules.anyOtherURL
import com.test.feedswithtests.TestModules.anyURL
import javax.inject.Inject

class ImageLoaderTest @Inject constructor() : ImageLoader {

    override fun loadInto(url: String, view: ImageView) {
        val color = when(url){
            anyURL() -> Color.BLUE
            anyOtherURL() -> Color.GREEN
            else -> Color.RED
        }
        val bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        canvas.drawColor(color)
        view.setImageBitmap(bitmap)
    }

    fun getBitmap(color: Int, view: ImageView): Bitmap {
        val bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        canvas.drawColor(color)
        view.setImageBitmap(bitmap)
        return view.drawToBitmap()
    }
}