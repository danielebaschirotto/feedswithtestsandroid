package com.test.feedswithtests.TestModules

import com.test.feedswithtests.network.APIManager
import javax.inject.Inject


data class Result (val code : Int, val data:Any?, val errorData: String?)

class APIManagerTest @Inject constructor() : APIManager {
    var feedCompletions = mutableListOf<Result>()
    var feedCallCount = 0

    companion object {
        var fail = false
    }

    override fun getFeeds(responseCallback: (Int, Any?, String?, String) -> Unit){
        if(!fail){
            responseCallback(201, anyFeedList(), "", "")
            feedCompletions.add(Result(201, anyFeedList(), ""))
        } else{
            responseCallback(404, null, "Generic Error", "")
            feedCompletions.add(Result(404, null, "Generic Error"))
        }
        feedCallCount++

    }
}