package com.test.feedswithtests.tests

import android.graphics.Color
import androidx.core.view.drawToBitmap
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.test.feedswithtests.imageLoader.ImageLoaderTest
import com.test.feedswithtests.TestModules.*
import com.test.feedswithtests.controllers.DetailsActivity
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest

import org.junit.Assert.*

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Rule

import androidx.test.core.app.ApplicationProvider

import android.content.Intent
import androidx.test.core.app.ActivityScenario


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class DetailsTestSuite {
    @get:Rule(order = 0)
    val hiltRule = HiltAndroidRule(this)



    @Test
    fun useAppContext() {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.test.feedswithtests", appContext.packageName)
    }


    @Test
    fun test_load_detailActivityWithExtraDisplayImageCorrectly() {
        val intent = Intent(ApplicationProvider.getApplicationContext(), DetailsActivity::class.java)
        intent.putExtra("feed", anyFeed())
        val scenario = ActivityScenario.launch<DetailsActivity>(intent)
        scenario.onActivity {
            assert(it.binding.image.drawToBitmap().sameAs(ImageLoaderTest().getBitmap(Color.BLUE, it.binding.image)))
        }
    }

    @Test
    fun test_load_detailActivityWithExtraDisplayDescriptionCorrectly() {
        val intent = Intent(ApplicationProvider.getApplicationContext(), DetailsActivity::class.java)
        intent.putExtra("feed", anyFeed())
        val scenario = ActivityScenario.launch<DetailsActivity>(intent)
        scenario.onActivity {
            assertTrue(it.binding.description.text.toString() == anyFeed().description)
        }
    }

    @Test
    fun test_load_detailActivityWithExtraDisplayLocationCorrectly() {
        val intent = Intent(ApplicationProvider.getApplicationContext(), DetailsActivity::class.java)
        intent.putExtra("feed", anyFeed())
        val scenario = ActivityScenario.launch<DetailsActivity>(intent)
        scenario.onActivity {
            assertTrue(it.binding.location.text.toString() == anyFeed().location)
        }
    }

}