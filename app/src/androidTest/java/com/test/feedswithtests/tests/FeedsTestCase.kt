package com.test.feedswithtests.tests

import android.graphics.Bitmap
import android.graphics.Color
import androidx.core.view.drawToBitmap
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.test.feedswithtests.controllers.FeedsActivity
import com.test.feedswithtests.DSL.clickOnCell
import com.test.feedswithtests.DSL.getCell
import com.test.feedswithtests.DSL.simulateUserLoad
import com.test.feedswithtests.imageLoader.ImageLoaderTest
import com.test.feedswithtests.models.Feed
import com.test.feedswithtests.TestModules.*
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest

import org.junit.Assert.*

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Rule

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class FeedsTestSuite {
    @get:Rule(order = 0)
    val hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    var activityScenarioRule = ActivityScenarioRule(FeedsActivity::class.java)

    @Test
    fun useAppContext() {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.test.feedswithtests", appContext.packageName)
    }

    @Test
    fun testInjection() {
        val scenario = activityScenarioRule.scenario
        scenario.onActivity {
            APIManagerTest.fail = false
            it.feedViewModel.getFeeds().observe(it, { result ->
                assertTrue(result.data != null)
                result.data?.let { feeds ->
                    assertTrue(feeds.containsAll(anyFeedList()))
                }
            })
        }
    }

    @Test
    fun test_load_invokesLoadingOnInitAndOnUserSimulatedLoad() {
        val scenario = activityScenarioRule.scenario
        scenario.onActivity {
            APIManagerTest.fail = false

            fun condition(value : Int) : Boolean{
                return (it.feedViewModel.apiManager as APIManagerTest).feedCallCount == value
            }
            assertTrue(condition(1))
            it.simulateUserLoad()
            assertTrue(condition(2))
            it.simulateUserLoad()
            assertTrue(condition(3))
        }
    }

    @Test
    fun test_isLoading_isDisplayedWhenLoadingAndDismissedOnResponse(){
        val scenario = activityScenarioRule.scenario
        scenario.onActivity {
            APIManagerTest.fail = false

            fun condition(value : Boolean): Boolean{
                return it.binding.swipe.isRefreshing == value
            }

            it.simulateUserLoad()
            Thread.sleep(1000)
            assertTrue(condition(false))
        }
    }

    @Test
    fun test_isLoading_isDisplayedWhenLoadingAndDismissedOnFail(){
        val scenario = activityScenarioRule.scenario
        scenario.onActivity {

            fun condition(value : Boolean): Boolean{
                return it.binding.swipe.isRefreshing == value
            }
            APIManagerTest.fail = true
            it.simulateUserLoad()
            Thread.sleep(1000)
            assertTrue(condition(false))
        }
    }

    @Test
    fun test_feed_isRenderedWhenFeedIsReceivedFromInitOrFromUserSimulatedLoad() {
        val scenario = activityScenarioRule.scenario
        scenario.onActivity {

            fun condition(cellNum:Int, item: Feed): Boolean {
                it.getCell(cellNum)?.let { cell ->
                    return (cell.binding.location.text.toString() == item.location)
                }
                return false

            }

            assertTrue(condition(0, anyFeed()))
            assertTrue(condition(1, anyOtherFeed()))
        }
    }

    @Test
    fun test_cells_renderReceivedImagesFromAppear() {
        val scenario = activityScenarioRule.scenario
        scenario.onActivity {

            it.getCell(0)?.let { cell ->
                assertTrue(cell.binding.itemImage.drawToBitmap().sameAs(ImageLoaderTest().getBitmap(Color.BLUE, cell.binding.itemImage)))
            }
            it.getCell(1)?.let { cell ->
                assertTrue(cell.binding.itemImage.drawToBitmap().sameAs(ImageLoaderTest().getBitmap(Color.GREEN, cell.binding.itemImage)))
            }
        }
    }

    @Test
    fun test_cell_tap_brings_to_detail_screen(){
        val scenario = activityScenarioRule.scenario
        scenario.onActivity {
            it.clickOnCell(0)

            fun condition(): Boolean {
                return EARouterSpy.messages.contains("details")
            }
            assertTrue(condition())
        }
    }

}