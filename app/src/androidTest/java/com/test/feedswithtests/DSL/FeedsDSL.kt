package com.test.feedswithtests.DSL

import com.test.feedswithtests.controllers.FeedsActivity
import com.test.feedswithtests.views.FeedCell

fun FeedsActivity.simulateUserLoad() {
    onRefresh()
}

fun FeedsActivity.getCell(pos: Int) : FeedCell?{
    return binding.feedsList.findViewHolderForAdapterPosition(pos) as FeedCell?
}

fun FeedsActivity.clickOnCell(pos: Int){
    getCell(pos)?.binding?.card?.performClick()
}