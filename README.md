# README #

Questo repository contiene un semplicissimo progetto Android dove approccio la struttura di un potenziale nuovo progetto.
L'esempio mostra l'integrazione con una API REST, l'utilizzo di Hilt per la DI e l'implementazione degli Acceptance tests.
Un focus particolare è dato proprio agli integration test ed all'utilizzo di Hilt per creare stub e spy per i test.


### Cosa implementa questo codice? ###

Questo codice implementa un banale contenitore di Feeds (nell'esempio luoghi con relative descrizioni).
L'app si collega ad una API REST (nel mio sito web) per mostrare la componente di rete,
sia per ottenere le informazioni che per visualizzare le immagini da remoto.
Usando lo swipe è possibile effettuare il refresh dei dati.
Il progetto punta anche e soprattutto a mostrare la parte di acceptance test per testare le funzionalità.
